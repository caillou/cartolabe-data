import bz2
import json
import logging
import os
import pickle

import pandas as pd
from gensim.corpora import Dictionary, MmCorpus
from gensim.matutils import corpus2csc
from sklearn.feature_extraction import text as sktxt
import mysql.connector as mariadb
import scipy.sparse as scs

from cartodata.exporting import Exporter
from cartodata.loading import PunctuationCountVectorizer
from cartodata.logs import timeit
from cartodata.operations import (
    normalize_tfidf, normalize_l2, load_scores, load_matrices_from_dumps
)
from cartodata.projection import lsa_projection, indirect_umap_projection
from cartodata.workflows.common import (
    dump_scores, create_clusters, dump_matrices, all_matrices_exist
)

logger = logging.getLogger(__name__)


def wiki_workflow(dump_dir):
    # do_workflow('datas/enwiki-20190120', 'en.z', 'enwiki', 25, 0.1,
    #             max_words=50000, num_dims=300, dump_dir=dump_dir)
    do_workflow('datas/simple', 'simple.z', 'simple_categories', 25, 0.1,
                max_words=20000, num_dims=300,
                dump_dir=dump_dir)


def filter_nan_articles(dump_dir):
    logger.info('Filtering NAN articles')
    natures = ['articles']
    articles_scores = load_scores(natures, dump_dir)[0]

    to_keep = articles_scores.index.notnull()

    articles_scores = articles_scores[to_keep]

    matrix = load_matrices_from_dumps(natures, 'umap', dump_dir)[0]
    matrix = matrix[:, to_keep]
    dump_matrices(natures, [matrix], 'umap', dump_dir)

    matrix = load_matrices_from_dumps(natures, 'lsa', dump_dir)[0]
    matrix = matrix[:, to_keep]
    dump_matrices(natures, [matrix], 'lsa', dump_dir)

    dump_scores(natures, [articles_scores], dump_dir)
    logger.info('Done filtering.')


def export(dump_dir, natures, neighbor_natures):
    exporter = Exporter(dump_dir, natures, neighbor_natures)
    exporter.export_to_feather()


def do_workflow(input_dir, project_code, db_name, min_df, max_df, max_words,
                num_dims, dump_dir):

    natures = ['articles', 'words']
    load_matrices(natures, input_dir, project_code, db_name, min_df, max_df,
                  max_words, dump_dir)

    matrices = load_matrices_from_dumps(natures, 'mat', dump_dir)
    do_projection(natures, matrices, num_dims, dump_dir)

    umap_matrices = load_matrices_from_dumps(natures, 'umap', dump_dir)
    lsa_matrices = load_matrices_from_dumps(natures, 'lsa', dump_dir)
    scores = load_scores(natures, dump_dir)

    filter_nan_articles(dump_dir)
    do_clustering(natures, umap_matrices, matrices, lsa_matrices, scores,
                  dump_dir)

    # power_scores = [0, 0]
    # get_all_neighbors(
    #    lsa_matrices, scores, power_scores, dump_dir, natures[:2]
    # )

    export(dump_dir, natures, natures[:2])


def load_matrices(natures, input_dir, project_code, db_name, min_df, max_df,
                  max_words, dump_dir, force=False):
    if all_matrices_exist(natures, 'mat', dump_dir) and not force:
        logger.info(
            f"Matrices for {', '.join(natures)} already exist. Skipping "
            "creation."
        )
        return

    gensim_loadwords(input_dir, min_df, max_df, max_words, db_name, dump_dir)
    gensim_tfidf(project_code, dump_dir)


def do_projection(natures, matrices, num_dims, dump_dir, force=False):
    if all_matrices_exist(natures, 'lsa', dump_dir) and not force:
        logger.info(
            f"LSA matrices for {', '.join(natures)} already exist. Skipping "
            "LSA."
        )
        lsa_matrices = load_matrices_from_dumps(natures, 'lsa', dump_dir)
    else:
        logger.info('Starting LSA projection')
        lsa_matrices = lsa_projection(num_dims, matrices[1], matrices)
        lsa_matrices = list(map(normalize_l2, lsa_matrices))
        dump_matrices(natures, lsa_matrices, 'lsa', dump_dir)

    if all_matrices_exist(natures, 'umap', dump_dir) and not force:
        logger.info(
            f"UMAP matrices for {', '.join(natures)} already exist. Skipping "
            "UMAP."
        )
    else:
        logger.info('Starting UMAP projection')
        umap_matrices = indirect_umap_projection(
            lsa_matrices, n_neighbors=5, min_dist=0.1, max_size=1500000
        )
        dump_matrices(natures, umap_matrices, 'umap', dump_dir)


def do_clustering(natures, umap_matrices, matrices, lsa_matrices, scores,
                  dump_dir):
    natures.extend(
        ['hl_clusters', 'ml_clusters', 'll_clusters', 'vll_clusters']
    )
    clus_lsa, clus_pos, clus_scores = create_clusters(
        natures[2:], umap_matrices[0], umap_matrices[1], matrices[1],
        scores[1], lsa_matrices[1], dump_dir, base_factor=4
    )
    umap_matrices.extend(clus_pos)
    scores.extend(clus_scores)


@timeit
def gensim_loadwords(top_dir, min_df, max_df, max_words, db_name, dump_dir):
    logger.info('Starting words loading')
    mariadb_connection = mariadb.connect(
        user='root', password='', database=db_name
    )
    cursor = mariadb_connection.cursor()

    with open('datas/stopwords.txt', 'r') as stop_file:
        stopwords = sktxt.ENGLISH_STOP_WORDS.union(
            set(stop_file.read().splitlines())
        )

    corpus = NgramWikiCorpus(top_dir, cursor, 4, stopwords)
    corpus.dictionary.filter_extremes(
        no_below=min_df, no_above=max_df, keep_n=max_words
    )
    corpus.dictionary.save_as_text(
        os.path.join(dump_dir, 'vocabulary.txt.bz2')
    )
    # save bag-of-words (term-document frequency matrix)
    MmCorpus.serialize(
        os.path.join(dump_dir, 'bow.mm'), corpus, progress_cnt=10000,
        metadata=True
    )


@timeit
def gensim_tfidf(project_code, dump_dir):
    logger.info('Loading corpus ...')
    corpus = MmCorpus(os.path.join(dump_dir, 'bow.mm'))

    logger.info('Creating articles2words matrix.')
    words_mat = corpus2csc(corpus).transpose()

    logger.info('TfIDF on words matrix.')
    words_mat = normalize_tfidf(words_mat)

    logger.info('Reading article titles.')
    # read articles titles
    with open(os.path.join(dump_dir, 'bow.mm.metadata.cpickle'), 'rb') as f:
        data = pickle.load(f)
        titles = [title for _, (_, title) in data.items()]

    articles_mat = scs.identity(len(titles))
    articles_scores = pd.Series(1, index=titles)

    dump_matrices(
        ['articles', 'words'], [articles_mat, words_mat], 'mat', dump_dir
    )

    logger.info('Loading dictionary ...')
    dictionary = Dictionary.load_from_text(
        os.path.join(dump_dir, 'vocabulary.txt.bz2')
    )

    logger.info('Creating words score vector.')
    words = [dictionary[idx] for idx in range(len(dictionary))]
    words_scores = pd.Series(words_mat.sum(axis=0).A1, index=words)

    logger.info('Reading counts...')
    iter_csv = pd.read_csv('datas/pagecounts-2018-12-views-ge-5-totals.bz2',
                           sep=' ', iterator=True, chunksize=10000,
                           header=None, names=['project', 'title', 'count'])

    for chunk in iter_csv:
        project_entries = chunk[chunk['project'] == project_code]
        for entry in project_entries.itertuples(index=None, name=None):
            try:
                title = entry[1]
                if type(title) == str:
                    title = title.replace('_', ' ')
                    articles_scores[title] += entry[2]
            except KeyError:
                pass

    dump_scores(
        ['articles', 'words'], [articles_scores, words_scores], dump_dir
    )


class NgramWikiCorpus(object):
    def __init__(self, top_dir, cursor, number_of_grams, stopwords,
                 article_min_tokens=50, dictionary=None,
                 dict_prune_at=10000000):
        self.top_dir = top_dir
        self.cursor = cursor
        self.metadata = False
        vectorizer = PunctuationCountVectorizer(
            ngram_range=(1, number_of_grams), strip_accents='unicode',
            stop_words=stopwords, lowercase=True,
            token_pattern='[a-zA-Z0-9]{5,}'
        )
        self.analyzer = vectorizer.build_analyzer()
        self.article_min_tokens = article_min_tokens

        if dictionary is None:
            self.dictionary = Dictionary(
                self.get_texts(), prune_at=dict_prune_at
            )
        else:
            self.dictionary = dictionary

    def init_dictionary(self, dictionary):
        """Initialize/update dictionary.
        Parameters
        ----------
        dictionary : :class:`~gensim.corpora.dictionary.Dictionary`, optional
            If a dictionary is provided, it will not be updated with the given
            corpus on initialization.
            If None - new dictionary will be built for the given corpus.
        Notes
        -----
        If self.input is None - make nothing.
        """
        self.dictionary = Dictionary(prune_at=500000)
        if dictionary is not None:
            self.dictionary = dictionary

        if self.top_dir is not None:
            if dictionary is None:
                logger.info("Initializing dictionary")
                metadata_setting = self.metadata
                self.metadata = False
                self.dictionary.add_documents(self.get_texts())
                self.metadata = metadata_setting
            else:
                logger.info(
                    "Input stream provided but dictionary already initialized"
                )
        else:
            logger.warning(
                "No input document stream provided; assuming dictionary will "
                "be initialized some other way."
            )

    def __iter__(self):
        """Iterate over the corpus.
        Yields
        ------
        list of (int, int)
            Document in BoW format (+ metadata if self.metadata).
        """
        if self.metadata:
            for text, metadata in self.get_texts():
                yield (
                    self.dictionary.doc2bow(text, allow_update=False), metadata
                )
        else:
            for text in self.get_texts():
                yield self.dictionary.doc2bow(text, allow_update=False)

    def get_texts(self):
        for doc in iter_documents(self.top_dir, self.cursor):
            tokens = self.analyzer(doc['text'])
            if len(tokens) < self.article_min_tokens:
                continue
            if not isinstance(doc['title'], str) or doc['title'] == "":
                continue
            if self.metadata:
                yield (tokens, (doc['id'], doc['title']))
            else:
                yield tokens


def iter_documents(top_directory, cursor=None):
    """
    Iterate over all documents, yielding one document at a time.
    If cursor is not None, use it to lookup categories associated with the
    document and add them to the text.

    :param top_directory:
    :param cursor:
    :return:
    """
    for root, dirs, files in os.walk(top_directory):
        for file in filter(lambda file: file.endswith('.bz2'), files):
            with bz2.BZ2File(os.path.join(root, file), 'r') as f:
                for line in f:
                    document = json.loads(line)
                    document['text'] = document['text'].replace('\n', ' ')
                    if cursor is not None:
                        cursor.execute(
                            "SELECT cl_to FROM categorylinks WHERE cl_from=%s",
                            (document['id'],)
                        )
                        categories = []
                        for (category,) in cursor:
                            categories.append(
                                category.decode('utf-8').replace('_', ' ')
                            )
                        if categories:
                            document['text'] += '. '.join(categories)

                    yield document
