import logging

import pandas as pd
from sklearn.feature_extraction import text as sktxt

from cartodata.exporting import Exporter
from cartodata.loading import (
    load_identity_column, load_comma_separated_column, load_text_column
)
from cartodata.operations import (
    normalize_tfidf, filter_min_score, load_matrices_from_dumps, load_scores
)
from cartodata.workflows.common import (
    get_data_from_csv_dump, create_clusters, get_all_neighbors, dump_scores,
    dump_matrices, do_projection, all_matrices_exist
)

logger = logging.getLogger(__name__)


def hal_workflow(dump_dir):
    df = get_data_from_csv_dump('datas/hal_2000_2020.csv')
    # do_workflow(df, 25, 0.1, 100000, 250000, 300, dump_dir, 4, 10)
    load_workflow(dump_dir, df)


def lri_workflow(dump_dir):
    df = get_data_from_csv_dump('datas/lri_2000_2019.csv')
    do_workflow(df, 10, 0.05, None, None, 200, dump_dir)


def inria_workflow(dump_dir):
    df = get_data_from_csv_dump('datas/inria_2000_2018.csv')
    do_workflow(df, 25, 0.1, None, None, 300, dump_dir)
    # load_workflow(dump_dir, df)


def ups_workflow(dump_dir):
    df = get_data_from_csv_dump('datas/ups_2000_2022.csv')
    do_workflow(df, 25, 0.1, None, None, 300, dump_dir)


def load_workflow(dump_dir, df):
    natures = ['articles', 'authors', 'words', 'teams', 'labs', 'hl_clusters',
               'ml_clusters', 'll_clusters', 'vll_clusters']

    export(dump_dir, df, natures, natures[:5])


def do_workflow(df, min_df, max_df, max_words, vocab_sample, num_dims,
                dump_dir, filt_min_score=4, n_neighbors=10):
    natures = ['articles', 'authors', 'words', 'teams', 'labs', 'hl_clusters',
               'ml_clusters', 'll_clusters', 'vll_clusters']

    load_matrices(natures[:5], df, min_df, max_df, max_words,
                  vocab_sample, dump_dir, filt_min_score, force=False)

    matrices = load_matrices_from_dumps(natures[:5], 'mat', dump_dir)
    do_projection(natures[:5], matrices, matrices[2],
                  num_dims, dump_dir, force=False)

    umap_matrices = load_matrices_from_dumps(natures[:5], 'umap', dump_dir)
    lsa_matrices = load_matrices_from_dumps(natures[:5], 'lsa', dump_dir)
    scores = load_scores(natures[:5], dump_dir)

    clus_lsa, clus_pos, clus_scores = create_clusters(
        natures[5:], umap_matrices[0], umap_matrices[2], matrices[2],
        scores[2], lsa_matrices[2], dump_dir, base_factor=3
    )
    umap_matrices.extend(clus_pos)
    scores.extend(clus_scores)

    power_scores = [0, 0.5, 0.5, 0, 0]
    get_all_neighbors(lsa_matrices, scores, power_scores,
                      dump_dir, natures[:5], n_neighbors)

    export(dump_dir, df, natures, natures[:5])


def load_matrices(natures, df, min_df, max_df, max_words, vocab_sample,
                  dump_dir, filt_min_score, force=False):
    if all_matrices_exist(natures, 'mat', dump_dir) and not force:
        logger.info(
            f'Matrices for {natures} already exist. Skipping creation.'
        )
        return

    logger.info('Starting data loading')
    # Create term-document matrices for the entites we need
    articles_tab, articles_scores = load_identity_column(df, 'en_title_s')
    authors_tab, authors_scores = load_comma_separated_column(
        df, 'authFullName_s'
    )

    with open('datas/inria-teams.csv') as teams_file:
        inria_teams = teams_file.read().split(',')

    teams_tab, teams_scores = load_comma_separated_column(
        df, 'structAcronym_s', whitelist=inria_teams
    )
    labs_tab, labs_scores = load_comma_separated_column(
        df, 'structAcronym_s', blacklist=inria_teams, filter_acronyms=True
    )

    logger.info('Starting words loading')
    with open('datas/stopwords.txt', 'r') as stop_file:
        stopwords = sktxt.ENGLISH_STOP_WORDS.union(
            set(stop_file.read().splitlines()))

    df['text'] = df['en_abstract_s'] + '.' + df['en_title_s'] + '.' + \
        df['en_keyword_s'].astype(str) + '.' + \
        df['en_domainAllCodeLabel_fs'].astype(str)

    words_tab, words_scores = load_text_column(
        df['text'], 4, min_df, max_df, vocab_sample, max_words,
        stopwords=stopwords
    )

    # Normalise words_tab
    words_tab = normalize_tfidf(words_tab)

    matrices = [articles_tab, authors_tab, words_tab, teams_tab, labs_tab]
    scores = [articles_scores, authors_scores,
              words_scores, teams_scores, labs_scores]

    # filter labs and authors with min score = 4
    filt_authors, filt_auth_scores = filter_min_score(
        matrices[1], scores[1], filt_min_score)
    matrices[1] = filt_authors
    scores[1] = filt_auth_scores

    filt_labs, filt_lab_scores = filter_min_score(
        matrices[4], scores[4], filt_min_score)
    matrices[4] = filt_labs
    scores[4] = filt_lab_scores

    dump_scores(natures, scores, dump_dir)
    dump_matrices(natures, matrices, 'mat', dump_dir)


def export(dump_dir, df, natures, neighbor_natures):
    df['year'] = df['producedDateY_i'].astype(str)
    df['url'] = df['halId_s'].fillna('')

    exporter = Exporter(dump_dir, natures, neighbor_natures)
    exporter.add_reference('articles', 'labs')
    exporter.add_reference('articles', 'teams')
    exporter.add_reference('authors', 'labs')
    exporter.add_reference('authors', 'teams')

    exporter.merge_metadata_values('articles', ['teams', 'labs'], 'labs')
    exporter.merge_metadata_values('authors', ['teams', 'labs'], 'labs')
    exporter.add_metadata_values('articles', df[['year', 'url']])
    exporter.add_metadata_values(
        'authors', build_authors_years_list(dump_dir, df))
    exporter.export_to_feather()


def build_authors_years_list(dump_dir, df):
    matrix = load_matrices_from_dumps(['authors'], 'mat', dump_dir)[0]
    rows, cols = matrix.T.nonzero()
    col = 0
    row = 0
    years = []
    for idx in range(len(rows)):
        if rows[idx] == row:
            continue
        years.append(','.join(str(y) for y in set(
            df['producedDateY_i'].iloc[cols[col:idx]].values)))
        row = rows[idx]
        col = idx
    years.append(','.join(
        str(y) for y in set(df['producedDateY_i'].iloc[cols[col:-1]].values))
    )
    return pd.DataFrame(years, columns=['year'])
