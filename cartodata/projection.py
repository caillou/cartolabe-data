import logging
import os

import numpy as np
import scipy.sparse as scs
from gensim.matutils import Sparse2Corpus, corpus2dense
from gensim.models import LdaModel, doc2vec
from gensim import utils
from sklearn.utils.extmath import randomized_svd
from umap import UMAP

from cartodata.logs import timeit
from cartodata.operations import save_matrix

logger = logging.getLogger(__name__)


def lsa_get_randomized_svd(nb_dims, canon_table, randomized_svd_n_iter=5):

    U, sigma, VT = randomized_svd(canon_table, n_components=nb_dims,
                                  n_iter=randomized_svd_n_iter,
                                  random_state=None)
    return U, sigma, VT


def matrix_dot(U, matrix, max_size=1000):
    if matrix.shape[1] < max_size:
        return np.asarray(np.transpose(U).dot(matrix.todense()))

    # matrix size is too big, making dot product separately on submatrices
    ns = int(matrix.shape[1] / max_size)
    totspace = None
    for i in range(0, ns + 1):
        start = i * max_size
        end = (i + 1) * max_size
        if i == ns:
            end = matrix.shape[1]

        ext = matrix[:, start:end].todense()
        sext = np.asarray(np.transpose(U).dot(ext))
        if i == 0:
            totspace = sext
        else:
            totspace = np.concatenate([totspace, sext], axis=1)

    return totspace


@timeit
def lsa_projection(nb_dims, canon_table, matrices, dump_dir=None):
    U, sigma, VT = lsa_get_randomized_svd(nb_dims, canon_table)

    if dump_dir is not None:
        save_matrix(VT, os.path.join(dump_dir, 'lsa_components'))

    projected_matrices = []
    for matrix in matrices:
        if type(matrix) == scs.dia_matrix:
            # assume identity matrix
            proj = np.asarray(np.transpose(U))
            projected_matrices.append(proj)
        else:
            proj = matrix_dot(U, matrix)
            projected_matrices.append(proj)

    return projected_matrices


@timeit
def lda_projection(nb_dims, canon_table_idx, matrices, update_every=0,
                   passes=20):

    corpus = Sparse2Corpus(
        sparse=matrices[canon_table_idx], documents_columns=False
    )
    model = LdaModel(
        corpus, num_topics=nb_dims, update_every=update_every, passes=passes
    )

    U = corpus2dense(model[corpus], nb_dims)

    projected_matrices = []
    for matrix in matrices:
        if type(matrix) == scs.dia_matrix:
            projected_matrices.append(U)
        else:
            proj = matrix_dot(np.transpose(U), matrix)
            projected_matrices.append(proj)

    return projected_matrices


@timeit
def doc2vec_projection(nb_dims, canon_table_idx, matrices, vocab_df, workers=3,
                       epochs=5, callbacks=(), batch_words=10000,
                       trim_rule=None, alpha=0.025, window=5, seed=1, hs=0,
                       negative=5, ns_exponent=0.75, cbow_mean=1,
                       min_alpha=0.0001, compute_loss=False, dm_mean=None,
                       dm=1, dbow_words=0, dm_concat=0, dm_tag_count=1):

    corpusvoc = [utils.simple_preprocess(row) for row in vocab_df]
    tagcorpusvoc = [
        doc2vec.TaggedDocument(row, [i]) for i, row in enumerate(corpusvoc)
    ]

    model = doc2vec.Doc2Vec(vector_size=nb_dims, workers=workers,
                            epochs=epochs, callbacks=callbacks,
                            batch_words=batch_words, trim_rule=trim_rule,
                            alpha=alpha, window=window, seed=seed, hs=hs,
                            negative=negative, ns_exponent=ns_exponent,
                            cbow_mean=cbow_mean, min_alpha=min_alpha,
                            compute_loss=compute_loss, dm_mean=dm_mean, dm=dm,
                            dbow_words=dbow_words, dm_concat=dm_concat,
                            dm_tag_count=dm_tag_count)
    model.build_vocab(tagcorpusvoc)
    model.train(tagcorpusvoc, total_examples=model.corpus_count,
                epochs=model.epochs)

    Ulist = [model.infer_vector(row) for row in corpusvoc]
    U = np.asanyarray(Ulist).T

    projected_matrices = []
    for matrix in matrices:
        try:
            proj = matrix_dot(np.transpose(U), matrix)
            projected_matrices.append(proj)
        except TypeError:
            projected_matrices.append(U)

    return projected_matrices


@timeit
def umap_projection(matrices, n_neighbors=15, min_dist=0.1):
    """
    Apply the UMAP fit_transform algorithm on all the matrices, reducing the
    number of features of each matrix to 2.
    This works only if the combined number of documents for all matrices
    is ~ < 2million. Otherwise, use the indirect_umap_projection method.

    :param matrices:
    :param n_neighbors:
    :param min_dist:
    :return:
    """
    shapes = [matrix.shape for matrix in matrices]
    global_matrix = np.asarray(np.hstack(matrices).T)

    reducer = UMAP(n_neighbors=n_neighbors, min_dist=min_dist, init='random')
    proj_umap = reducer.fit_transform(global_matrix)

    min_coords = np.amin(proj_umap, axis=0)
    max_coords = np.amax(proj_umap, axis=0)
    logger.debug(
        f"UMAP projection done. Max coords: {max_coords}. "
        f"Min Coords: {min_coords}."
    )

    start = 0
    projected_matrices = []
    for shape in shapes:
        matrix = np.asarray(proj_umap[start:start + shape[1]].T)
        projected_matrices.append(matrix)
        start += shape[1]

    return projected_matrices


@timeit
def guided_umap_projection(projected_matrice, matrices, n_neighbors=15,
                           min_dist=0.1, max_size=2000000):
    """
    Apply the UMAP fit algorithm to the fprojected_matrice. Then transform
    all the matrices using the fitted model to extrapolate the vectors in 2
    dimension.

    :param projected_matrice:
    :param matrices:
    :param n_neighbors:
    :param min_dist:
    :param max_size:
    :return:
    """
    model = fit_transform_guided(projected_matrice, n_neighbors, min_dist,
                                 max_size)

    logger.debug(
        f"UMAP: Reference matrix projected to size {projected_matrice.shape}"
    )

    projected_matrices = []
    if len(matrices) > 0:
        for matrix in matrices:
            projected_matrix = umap_transform(model, matrix, max_size)
            projected_matrices.append(projected_matrix.T)
            logger.debug(
                f"UMAP: Matrix projected to size {projected_matrix.shape}"
            )

    # Check min and max coordinates
    stack = np.hstack(projected_matrices)
    min_coords = np.amin(stack, axis=1)
    max_coords = np.amax(stack, axis=1)
    logger.debug(
        f"UMAP projection done. Max coords: {max_coords}. "
        f"Min Coords: {min_coords}."
    )

    return projected_matrices


def fit_transform_guided(projected_matrice, n_neighbors, min_dist, max_size):
    """
    Fit a UMAP model with a random sample of max_size documents from the
    matrix. Then use this model to transform the remaining documents,
    combining the results into a transformed matrix that is returned.

    :param matrix:
    :param n_neighbors:
    :param min_dist:
    :param max_size:
    :return:
    """
    # Create a random permutation

    # Fit model on random mask data
    logger.debug(
        "UMAP: Fitting model with sample matrix of size "
        f"{projected_matrice.shape}"
    )
    model = UMAP(n_neighbors=n_neighbors, min_dist=min_dist, init='random')
    model.fit(projected_matrice.T)

    # Model's embedding is equal to transformed data for the train data
    # Transform data for remaining documents
    return model


@timeit
def indirect_umap_projection(matrices, n_neighbors=15, min_dist=0.1,
                             max_size=2000000):
    """
    Apply the UMAP fit algorithm to the first matrix, with a limited random
    sample of max_size documents. Then transform all the matrices using the
    fitted model to extrapolate the vectors in 2 dimension.

    :param matrices:
    :param n_neighbors:
    :param min_dist:
    :param max_size:
    :return:
    """
    fit_matrix = matrices[0]
    if fit_matrix.shape[1] > max_size:
        model, model_matrix = fit_transform_big_matrix(fit_matrix, n_neighbors,
                                                       min_dist, max_size)
    else:
        logger.debug(
            f"UMAP: Fitting model with full matrix of size {fit_matrix.shape}"
        )
        model = UMAP(n_neighbors=n_neighbors, min_dist=min_dist, init='random')
        model.fit(fit_matrix.T)

        model_matrix = model.embedding_

    logger.debug(
        f"UMAP: Reference matrix projected to size {model_matrix.shape}"
    )

    projected_matrices = [model_matrix.T]
    if len(matrices) > 1:
        for matrix in matrices[1:]:
            projected_matrix = umap_transform(model, matrix, max_size)
            projected_matrices.append(projected_matrix.T)
            logger.debug(
                f"UMAP: Matrix projected to size {projected_matrix.shape}"
            )

    # Check min and max coordinates
    stack = np.hstack(projected_matrices)
    min_coords = np.amin(stack, axis=1)
    max_coords = np.amax(stack, axis=1)
    logger.debug(f"UMAP projection done. Max coords: {max_coords}. "
                 f"Min Coords: {min_coords}.")

    return projected_matrices


def fit_transform_big_matrix(matrix, n_neighbors, min_dist, max_size):
    """
    Fit a UMAP model with a random sample of max_size documents from the
    matrix. Then use this model to transform the remaining documents,
    combining the results into a transformed matrix that is returned.

    :param matrix:
    :param n_neighbors:
    :param min_dist:
    :param max_size:
    :return:
    """
    # Create a random permutation
    perm = np.random.permutation(matrix.shape[1])
    fit_data = matrix[:, perm[:max_size]]

    # Fit model on random mask data
    logger.debug(
        f"UMAP: Fitting model with sample matrix of size {fit_data.shape}"
    )
    model = UMAP(n_neighbors=n_neighbors, min_dist=min_dist, init='random')
    model.fit(fit_data.T)

    # Model's embedding is equal to transformed data for the train data
    fit_transformed = model.embedding_
    # Transform data for remaining documents
    other_transformed = umap_transform(
        model, matrix[:, perm[max_size:]], max_size
    )

    # Merge arrays
    merged = np.vstack((fit_transformed, other_transformed))
    # Rearrange in correct order
    res = merged[invert_permutation(perm), :]
    return model, res


def umap_transform(model, matrix, max_size, max_tries=5):
    """
    Transform a matrix with a given UMAP model, either straight away if the
    matrix has less than max_size documents, or by splitting it in sub
    matrices if it is too big. UMAP does not work well with class imbalances.
    To avoid that, we randomly permute the matrix, to try to randomly
    distribute classes across all submatrices.

    :param max_tries:
    :param model:
    :param matrix:
    :param max_size:
    :return:
    """
    logger.debug(f"UMAP: Transforming matrix of size {matrix.shape}")
    if matrix.shape[1] > max_size:
        tries = 0
        while tries < max_tries:
            try:
                projected_matrix = split_transform(model, matrix, max_size)
                tries = max_tries
            except ValueError as e:
                logger.debug(
                    'UMAP: Transformation of submatrices failed due to invalid'
                    'permutation.'
                )
                tries += 1
                if tries < max_tries:
                    logger.debug('UMAP: Retrying transformation...')
                else:
                    raise e
    else:
        projected_matrix = model.transform(matrix.T)

    return projected_matrix


def split_transform(model, matrix, max_size):
    nb_seg = int(matrix.shape[1] / max_size) + 1
    seg_size = int(matrix.shape[1] / nb_seg)
    perm = np.random.permutation(matrix.shape[1])
    projected_matrix = None
    for i in range(nb_seg):
        start = i * seg_size
        end = matrix.shape[1] if i == nb_seg - 1 else start + seg_size

        logger.debug(
            f"UMAP: Transforming sub matrix with cols from {start} to {end}"
        )
        proj = model.transform(matrix[:, perm[start:end]].T)
        if i == 0:
            projected_matrix = proj
        else:
            projected_matrix = np.concatenate([projected_matrix, proj], axis=0)

    return projected_matrix[invert_permutation(perm), :]


def invert_permutation(p):
    """
    The argument p is assumed to be some permutation of 0, 1, ..., len(p)-1.
    Returns an array s, where s[i] gives the index of i in p.
    """
    s = np.empty(p.size, p.dtype)
    s[p] = np.arange(p.size)
    return s
