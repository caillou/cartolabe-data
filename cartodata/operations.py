import gzip
import json
import logging
import os
import warnings
import pickle

import numpy as np
import pandas as pd
import scipy.sparse as scs
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.preprocessing import Normalizer, StandardScaler

from cartodata.neighbors import NEIGHBORS_FILENAME_FORMAT

logger = logging.getLogger(__name__)


def normalize_tfidf(matrix):
    return TfidfTransformer().fit_transform(matrix)


def normalize_l2(matrix, axis=1):
    if axis == 1:
        return Normalizer(norm="l2").fit_transform(matrix.T).T

    return StandardScaler().fit_transform(matrix.T).T


def filter_min_score(matrix, scores, min_score):
    indices_to_keep = np.where(scores > min_score)[0]
    filtered_matrix = matrix[:, indices_to_keep]
    filtered_scores = scores[indices_to_keep]

    return filtered_matrix, filtered_scores


def export_to_json(natures, positions, scores, filename,
                   neighbors_natures=None, dump_dir=None, neighbors=None):
    total = 0
    points = []

    for idx, nature in enumerate(natures):
        elements = create_elements_list(
            nature, positions[idx], scores[idx], total
        )
        points.extend(elements)

    if neighbors_natures is not None:
        add_neighbors_info(
            points, neighbors_natures, scores, dump_dir, neighbors
        )

    with open(filename, 'w') as f:
        json.dump(points, f)


def create_elements_list(nature, positions, scores, total):
    nb_elements = positions.shape[1]
    elements = []

    for idx in range(nb_elements):
        element = {
            'position': positions[:, idx].tolist(),
            'score': float(scores.values[idx]),
            'rank': total,
            'nature': nature,
            'label': str(scores.index[idx])
        }
        elements.append(element)
        total += 1

    return elements


def add_neighbors_info(points, natures, scores, dump_dir, neighbors):
    total = 0
    offsets = [0]

    for score in scores:
        offsets.append(offsets[-1] + len(score))

    for nature_idx, point_nature in enumerate(natures):
        if neighbors is None:
            nature_neighbors = load_neighbors_for_nature(point_nature, natures,
                                                         dump_dir)
        else:
            nature_neighbors = [n[nature_idx] for n in neighbors]

        for point_idx in range(len(scores[nature_idx])):
            point = points[total]
            p_neighbors = {}

            for k, nature in enumerate(natures):
                point_nature_neighbors = nature_neighbors[k][:, point_idx] + (
                    offsets[k]
                )
                p_neighbors[nature] = point_nature_neighbors.tolist()

            point['neighbors'] = p_neighbors
            total += 1


def load_neighbors_for_nature(point_nature, natures, dump_dir):
    nature_neighbors = []

    for nature in natures:
        filename = os.path.join(
            dump_dir, NEIGHBORS_FILENAME_FORMAT.format(nature, point_nature)
        )

        try:
            with gzip.GzipFile(filename, 'r') as file:
                matrix = np.load(file)
                nature_neighbors.append(matrix)
        except FileNotFoundError:
            matrix = np.load(filename[:-3])
            nature_neighbors.append(matrix)
        except IOError as e:
            warnings.warn(e)
    return nature_neighbors


def dump_matrices(natures, matrices, suffix, dirname):
    for idx, nature in enumerate(natures):
        filename = save_matrix(
            matrices[idx], os.path.join(dirname, nature + '_' + suffix)
        )
        logger.info(
            f"Saving {nature} matrix of size {matrices[idx].shape} into "
            f"{filename}."
        )


def save_matrix(matrix, filename):
    if scs.issparse(matrix):
        filename += '.npz'
        scs.save_npz(filename, matrix)
    else:
        filename += '.npy.gz'
        with gzip.GzipFile(filename, 'w') as file:
            np.save(file, matrix)

    return filename


def dump_scores(natures, scores, dirname):
    for idx, nature in enumerate(natures):
        filename = os.path.join(dirname, nature + '_scores')
        logger.info(
            f"Saving {nature} score vector of length {len(scores[idx])} into "
            f"{filename}."
        )
        with gzip.GzipFile(filename + '.npy.gz', 'w') as file:
            np.save(file, scores[idx].values)

        scores[idx].to_csv(filename + '.csv.bz2', columns=[], header=False,
                           compression='bz2')


def load_scores(natures, dirname):
    try:
        return deprecated_load_scores(natures, dirname)
    except KeyError:
        scores = []
        for nature in natures:
            filename = os.path.join(dirname, nature + '_scores')
            with gzip.GzipFile(filename + '.npy.gz', 'r') as file:
                values = np.load(file)

            index = pd.read_csv(
                filename + '.csv.bz2', index_col=0, header=None,
                compression='bz2'
            )
            score = pd.Series(values, index=index.index.values)
            logger.info(
                f"Loaded {nature} score vector of length {len(score)}."
            )
            scores.append(score)

        return scores


def deprecated_load_scores(natures, dirname):
    scores = []
    for nature in natures:
        filename = os.path.join(dirname, nature + '_scores.csv.bz2')
        score = pd.read_csv(
            filename, index_col=0, names=['scores'], compression='bz2'
        )['scores']

        logger.info(
            f"Loaded {nature} score vector of length {len(score)}."
        )
        scores.append(score)

    return scores


def load_matrices_from_dumps(natures, suffix, dirname):
    matrices = []

    for nature in natures:
        filename = os.path.join(dirname, nature + '_' + suffix)
        try:
            matrix = scs.load_npz(filename + '.npz')
        except FileNotFoundError:
            with gzip.GzipFile(filename + '.npy.gz', 'r') as file:
                matrix = np.load(file)

        matrices.append(matrix)
        logger.info(
            f"Loaded {nature} matrix of size {matrix.shape}."
        )

    return matrices


def dump_objects(natures, scores, objname, dirname):
    for idx, nature in enumerate(natures):
        filename = os.path.join(dirname, nature + '_' + objname + '.pickle')

        logger.info(
            f"Saving {nature} object vector into {filename}."
        )

        with open(filename, 'wb') as output:
            pickle.dump(scores[idx], output)


def load_objects(natures, objname, dirname):
    scores = []
    for nature in natures:
        filename = os.path.join(dirname, nature + '_' + objname + '.pickle')

        with open(filename, 'rb') as input:
            score = pickle.load(input)
            scores.append(score)

        logger.info(f"Loaded {nature} object vector.")

    return scores
