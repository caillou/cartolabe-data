import gzip
import json
import logging
import os
from typing import List

import pandas as pd
import numpy as np

from cartodata.neighbors import NEIGHBORS_FILENAME_FORMAT
from cartodata.operations import load_scores, load_matrices_from_dumps

logger = logging.getLogger(__name__)


class InvalidLengthException(Exception):
    pass


class Exporter(object):
    """
    Class to manage exporting results to a single file, either JSON format or
    Feather format.
    """

    def __init__(self, dump_dir, natures, neighbor_natures=None):
        """
        :param dump_dir: The directory where all the matrices / scores are
        stored
        :param natures: The list of point natures
        :param neighbor_natures: The list of neighbor natures
        """

        self.natures = natures
        self.neighbor_natures = []
        if neighbor_natures is not None:
            self.neighbor_natures = neighbor_natures
        self.dump_dir = dump_dir
        self.metadata = {}
        self._init_points()

    def _init_points(self):
        points = []
        self.counts = []
        total = 0

        for nature in self.natures:
            nature_points = create_nature_points(nature, self.dump_dir, total)
            self.counts.append(len(nature_points))
            total += self.counts[-1]
            points.extend(nature_points)

        self.points = pd.DataFrame(points)
        self.points['label'].fillna('N/A', inplace=True)

    def export_to_feather(self, filename=None):
        if filename is None:
            filename = os.path.join(self.dump_dir, 'export.feather')

        neighbor_matrices = self._get_neighbor_matrices()
        export = []
        metadata_gen = self._gen_metadata()

        for row in self.points.itertuples(index=False):
            element = row._asdict()
            p_neighbors = {
                'nn_' + nature: (
                    ','.join(map(
                        str,
                        neighbor_matrices[idx][element['rank'], :].tolist()
                    ))
                ) for idx, nature in enumerate(self.neighbor_natures) if (
                    element['rank'] < neighbor_matrices[idx].shape[0]
                )
            }
            if p_neighbors:
                element.update(p_neighbors)

            metadata = next(metadata_gen)
            if metadata is not None:
                element.update(metadata._asdict())
            export.append(element)

        pd.DataFrame(export).to_feather(filename)

    def export_to_hdf(self, filename=None):
        """
        Export results to HDF5 format.

        :param filename:
        :return:
        """
        if filename is None:
            filename = os.path.join(self.dump_dir, 'export.h5')

        with pd.HDFStore(filename, mode='w', complib='blosc:snappy',
                         complevel=9) as store:
            store.append('points', self.points)

            neighbor_matrices = self._get_neighbor_matrices()
            for idx, nature in enumerate(self.neighbor_natures):
                store.append('points/%s' %
                             nature, pd.DataFrame(neighbor_matrices[idx]))

            for nature, metadata in self.metadata.items():
                store.append('metadata/%s' % nature, metadata)

    def export_to_json(self, filename=None):
        """
        Export results to JSON format.

        :param filename:
        :return:
        """
        if filename is None:
            filename = os.path.join(self.dump_dir, 'export.json')

        neighbor_matrices = self._get_neighbor_matrices()
        export = []
        self.points['position'] = self.points[['x', 'y']].values.tolist()
        points = self.points.drop(['x', 'y'], axis=1)
        metadata_gen = self._gen_metadata()

        for row in points.itertuples(index=False):
            element = row._asdict()
            p_neighbors = {
                nature: (
                    neighbor_matrices[idx][element['rank'], :].tolist()
                ) for idx, nature in enumerate(self.neighbor_natures) if (
                    element['rank'] < neighbor_matrices[idx].shape[0]
                )
            }

            if p_neighbors:
                element['neighbors'] = p_neighbors

            metadata = next(metadata_gen)
            if metadata is not None:
                element.update(metadata._asdict())
            export.append(element)

        with open(filename, 'w') as f:
            json.dump(export, f)

    def _get_nature_offset(self, nature):
        return sum(self.counts[:self.natures.index(nature)])

    def _get_neighbor_matrices(self):
        offsets = [0]
        for count in self.counts:
            offsets.append(offsets[-1] + count)

        matrices = []
        for nature in self.neighbor_natures:
            neighbors = load_nature_neighbors(
                nature, self.neighbor_natures,
                offsets[self.neighbor_natures.index(nature)],
                self.dump_dir
            )
            matrices.append(neighbors)
        return matrices

    def _gen_metadata(self):
        for idx, nature in enumerate(self.natures):
            if nature in self.metadata:
                for row in self.metadata[nature].itertuples(index=False):
                    yield row
            else:
                for _ in range(self.counts[idx]):
                    yield None

    def add_reference(self, from_nature, to_nature):
        """
        For each point of nature `from_nature`, adds a list of `to_nature`
        points connected to it. For example,
        .add_reference('articles', 'labs') will add the list of labs connected
        to each article.

        :param from_nature:
        :param to_nature:
        :return:
        """
        natures = [from_nature, to_nature]
        matrices = load_matrices_from_dumps(natures, 'mat', self.dump_dir)
        matrix = matrices[0].T * matrices[1]

        rows, cols = matrix.nonzero()
        col = 0
        row = 0
        offset = self._get_nature_offset(to_nature)
        references = pd.DataFrame(index=range(matrix.shape[0]))

        for idx in range(len(rows)):
            if rows[idx] == row:
                continue

            references.at[row, to_nature] = (
                ','.join(str(x) for x in (cols[col:idx] + offset).tolist())
            )
            row = rows[idx]
            col = idx

        references.at[rows[-1], to_nature] = ','.join(
            str(x) for x in (cols[col:-1] + offset).tolist()
        )
        references[to_nature].fillna('', inplace=True)

        self.add_metadata_values(from_nature, references)

    def add_metadata_values(self, nature: str, values: pd.DataFrame):
        """
        Adds the values in values to the points of type `nature`.
        All the values should be of type string for compatibility.

        :param nature: the nature of points to update
        :param values: A dataframe with values as columns.
        :return:
        """
        if len(values) != self.counts[self.natures.index(nature)]:
            raise InvalidLengthException(
                f"Metadata values length ({len(values)}) for {nature} do not "
                f"match {nature} length "
                f"({self.counts[self.natures.index(nature)]})"
            )

        values.index = pd.Series(range(len(values)))
        if nature in self.metadata:
            self.metadata[nature] = pd.concat(
                [self.metadata[nature], values], axis=1)
        else:
            self.metadata[nature] = values

    def merge_metadata_values(self, nature: str, columns: List[str],
                              join_column_name: str):
        """
        Merge the metadata columns for points of type `nature`
        :param nature:
        :param columns:
        :param join_column_name:
        :return:
        """
        df = self.metadata[nature]
        df[columns].fillna('', inplace=True)
        df[join_column_name] = df[columns].apply(','.join, axis=1)
        to_drop = [col for col in columns if col != join_column_name]
        df.drop(to_drop, axis=1, inplace=True)


def load_nature_neighbors(point_nature, natures, offset, dump_dir):
    nature_neighbors = []
    for nature in natures:
        filename = os.path.join(
            dump_dir, NEIGHBORS_FILENAME_FORMAT.format(point_nature, nature))
        try:
            with gzip.GzipFile(filename, 'r') as file:
                matrix = np.load(file)
                nature_neighbors.append(np.transpose(matrix + offset))
        except FileNotFoundError:
            matrix = np.load(filename[:-3])
            nature_neighbors.append(np.transpose(matrix + offset))
        except IOError as e:
            logger.warning(e)
            return None
    return np.vstack(nature_neighbors)


def create_nature_points(nature, dump_dir, index_offset):
    scores = load_scores([nature], dump_dir)[0]
    positions = load_matrices_from_dumps([nature], 'umap', dump_dir)[0]
    nb_elements = positions.shape[1]
    elements = []
    for idx in range(nb_elements):
        position = positions[:, idx].tolist()
        element = {
            'nature': nature,
            'label': str(scores.index[idx]),
            'score': float(scores.values[idx]),
            'rank': index_offset + idx,
            'x': position[0],
            'y': position[1]
        }
        elements.append(element)
    return elements
