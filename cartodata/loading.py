import itertools
import re

import pandas as pd
import numpy as np
import scipy.sparse as scs
from sklearn.feature_extraction.text import CountVectorizer, TfidfTransformer
from unidecode import unidecode

from cartodata.logs import timeit

NON_ALPHANUMERIC_REGEX = re.compile('[^a-zA-Z0-9 ]+')
PHRASE_SEPARATOR_REGEX = re.compile('[,.;]+')


@timeit
def load_identity_column(df, column):
    """
    Create an identity term-frequency matrix from a dataframe column.
    :param df: The dataframe
    :param column: The column to map as identity
    :return: an identity matrix of n_samples x n_samples size.
    """
    tab = scs.identity(len(df[column]))
    scores = pd.Series(tab.diagonal(), index=df[column].values)
    return tab, scores


def normalize_term(term, whitelist=None, blacklist=None, banned_keywords=None,
                   filter_acronyms=False):
    """
    Normalize a term for text analysis. This method returns None if the
    normalized term is empty or should not be used, otherwise it returns
    the normalized term.

    :param term: The term to normalize.
    :param whitelist: a list of valid terms. If normalized term is not in this
    list, returns None.
    :param blacklist: a list of invalid terms. If normalized term is in this
    list, returns None.
    :param banned_keywords: a list of banned keywors. If normalized term is in
    this list, return None.
    :param filter_acronyms: boolean. If filter_acronyms is True, only
    normalized terms less than 11 chars will be kept.
    :return: the normalized term.
    """
    if term is None:
        return None

    normalized_term = NON_ALPHANUMERIC_REGEX.sub('', unidecode(term))
    normalized_term = normalized_term.lower().strip()

    if len(normalized_term) == 0:
        return None
    if blacklist is not None and normalized_term in blacklist:
        return None
    if whitelist is not None and normalized_term not in whitelist:
        return None
    if banned_keywords is not None and normalized_term in banned_keywords:
        return None
    if filter_acronyms and len(normalized_term) > 14:
        return None

    return normalized_term


@timeit
def load_comma_separated_column(df, column, whitelist=None, blacklist=None,
                                banned_keywords=None,
                                filter_acronyms=False, comma=','):
    """
    Creates a term-frequency matrix from a data column containing comma
    separated values. The column in the dataframe should contain n_samples
    rows of type string. Each row should be one string with comma separated
    values.
    Occurrences of unique terms are counted by row in the resulting matrix.

    :param df: the dataframe.
    :param column: the column with comma separated values
    :param whitelist: optional. A list of only terms allowed.
    :param blacklist: optional. A list of forbidden terms.
    :param banned_keywords: optional. A list of forbidden terms.
    :param filter_acronyms: optional. boolean to keep only terms that are less
    than 11 chars long.
    :param comma: optional. A character separator, defaults to a comma but can
    be a color or semi.
    :return: a pair: the term-frequency matrix of size n_samples x n_terms and
    the term count.
    """
    # split the values by comma
    rows = df[column].astype(str).str.split(comma)

    # build a term-document matrix incrementally
    # (see https://docs.scipy.org/doc/scipy/reference/generated/scipy.sparse.csr_matrix.html) # noqa
    indptr = [0]
    indices = []
    data = []
    vocabulary = {}
    column_labels = []

    for row in rows:
        for term in row:
            normalized_term = normalize_term(term, whitelist, blacklist,
                                             banned_keywords, filter_acronyms)
            if normalized_term is not None:
                index = vocabulary.get(normalized_term, None)
                if index is None:
                    index = len(vocabulary)
                    vocabulary[normalized_term] = index
                    column_labels.append(term.strip())
                indices.append(index)
                data.append(1)
        indptr.append(len(indices))

    tab = scs.csr_matrix((data, indices, indptr), dtype=int)

    scores = pd.Series(tab.sum(axis=0).A1, index=column_labels)
    return tab, scores


@timeit
def load_text_column(corpus, number_of_grams, min_df, max_df,
                     vocabulary_training_sample=None, max_features=None,
                     strip_accents='unicode', lowercase=True,
                     stopwords=None, min_word_length=5):
    if stopwords is not None:
        stopwords = list(stopwords)
    token_patern = '[a-zA-Z0-9]{' + str(min_word_length) + ',}'
    vectorizer = PunctuationCountVectorizer(
        min_df=min_df, max_df=max_df, ngram_range=(1, number_of_grams),
        max_features=max_features, strip_accents=strip_accents,
        stop_words=stopwords, lowercase=lowercase,
        token_pattern=token_patern
    )

    if vocabulary_training_sample is None:
        vectorizer.fit(corpus)
    else:
        vectorizer.fit(corpus.sample(vocabulary_training_sample))

    tab = vectorizer.transform(corpus)

    # scores = pd.Series(tab.sum(axis=0).A1,
    # index=vectorizer.get_feature_names())
    scores = pd.Series(np.bincount(tab.indices, minlength=tab.shape[1]),
                       index=vectorizer.get_feature_names_out())
    return tab, scores


def transform_new_docs(words_score, docs, number_of_grams,
                       strip_accents='unicode', lowercase=True,
                       stopwords=None, min_word_length=5,
                       n_samples_init=None):
    """
    Use a CountVectorizer initialized with the words_score array to tokenize
    new documents. If n_samples_init is not
    None, the counts are normalized by TfIdf.

    :param words_score: The words_score array
    :param docs: An iterable of new text documents
    :param number_of_grams: The max n-gram range
    :param strip_accents:
    :param lowercase:
    :param stopwords:
    :param min_word_length:
    :param n_samples_init: The number of documents the words_score vector was
    set with. This is the number of documents in the initial corpus. Needed
    for TfIDF normalization.
    :return:
    """
    token_patern = '[a-zA-Z0-9]{' + str(min_word_length) + ',}'
    vectorizer = PunctuationCountVectorizer(
        ngram_range=(1, number_of_grams), strip_accents=strip_accents,
        stop_words=stopwords, lowercase=lowercase, token_pattern=token_patern,
        vocabulary=words_score.index
    )
    vector = vectorizer.transform(docs)

    if n_samples_init is not None:
        # do TfIDF norm
        idf = np.log((n_samples_init + 1) / (words_score.values + 1)) + 1
        n_features = len(words_score)
        transformer = TfidfTransformer()
        transformer._idf_diag = scs.diags(idf, offsets=0,
                                          shape=(n_features, n_features),
                                          format='csr')
        vector = transformer.transform(vector)

    return vector


class PunctuationCountVectorizer(CountVectorizer):
    """
    In our data the keywords and domains of a document are separated by commas.
    We want to avoid building n-grams out of different keywords: "Mathematics,
    Computer Science" should only produce "mathematics" or "computer science"
    as n-grams and not "mathematics computer science".
    The custom analyzer for this PunctuationCountVectorizer splits the document
    on "[.,;]" and builds n-grams from each segment separately.
    It could be a better idea to use a custom separator for the keywords and
    domains instead of the comma and split on it but this hasn't been
    implemented yet.
    """

    def build_analyzer(self):
        preprocess = self.build_preprocessor()
        stop_words = self.get_stop_words()
        tokenize = self.build_tokenizer()
        self._check_stop_words_consistency(stop_words, preprocess, tokenize)

        # create the analyzer that will be returned by this method
        def analyser(doc):
            # Split document by phrases.
            phrases = re.split(PHRASE_SEPARATOR_REGEX, self.decode(doc))
            # Analyze phrases
            phrases_tokens = map(lambda doc: self._word_ngrams(
                tokenize(preprocess(self.decode(doc))), stop_words
            ), phrases)
            # Return flattened list of n_grams
            return list(itertools.chain(*phrases_tokens))

        return (analyser)
