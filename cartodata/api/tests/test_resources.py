from unittest.mock import MagicMock, patch, ANY

import falcon
import numpy as np
from falcon import testing
from pyfakefs.fake_filesystem_unittest import TestCase

from cartodata.api.api import get_app
from cartodata.api.configurations.hal import HalConfiguration


class TestResources(TestCase):

    def setUp(self) -> None:
        self.mock_store = MagicMock()
        self.mock_confs = {'inria': HalConfiguration}
        api = get_app(self.mock_store, self.mock_confs)
        self.client = testing.TestClient(api)

    def test_api_conf(self):
        # test invalid url
        response = self.client.simulate_get('/hello')
        self.assertEqual(response.status, falcon.HTTP_404)

        response = self.client.simulate_get('/wikipedia/position')
        self.assertEqual(response.status, falcon.HTTP_501)

        response = self.client.simulate_post('/wikipedia/neighbors')
        self.assertEqual(response.status, falcon.HTTP_501)

    def test_positions_resource(self):
        mock_res = np.array([[12.34, 0.9, 8.76]]), np.array([[3.4, -9.87]])
        with patch.object(
                self.mock_confs['inria'], 'project', return_value=mock_res
        ) as mock_project:
            response = self.client.simulate_get(
                '/inria/position?query=algorithms'
            )
            self.assertEqual(response.status, falcon.HTTP_200)
            mock_project.assert_called_with('algorithms')
            self.assertEqual(
                response.json, {'latent': mock_res[0].tolist(),
                                'projection': mock_res[1].tolist()}
            )

        mock_res = None, np.array([[3.4, -9.87]])
        with patch.object(
                self.mock_confs['inria'], 'project', return_value=mock_res
        ) as mock_project:
            response = self.client.simulate_get(
                '/inria/position?query=algorithms'
            )
            self.assertEqual(response.status, falcon.HTTP_200)
            mock_project.assert_called_with('algorithms')
            self.assertEqual(response.json, {'error': 'Unknown tokens'})

    def test_neighbors_resource(self):
        mock_res = [[100, 98, 1876, 0, 12]]
        json_body = {'vector': [[12.34, 0.9, 8.76]], 'nature': 'labs', 'n': 5}

        with patch.object(
                self.mock_confs['inria'], 'neighbors', return_value=mock_res
        ) as mock_neighbors:
            response = self.client.simulate_post(
                '/inria/neighbors', json=json_body
            )
            self.assertEqual(response.status, falcon.HTTP_200)
            mock_neighbors.assert_called_with(ANY, 'labs', 5)
            self.assertEqual(response.json, {'neighbors': mock_res})

        mock_res = [[100, 98, 1876, 0, 12]]
        json_body = {'nature': 'labs', 'n': 5}

        with patch.object(
                self.mock_confs['inria'], 'neighbors', return_value=mock_res
        ) as mock_neighbors:
            response = self.client.simulate_post(
                '/inria/neighbors', json=json_body
            )
            self.assertEqual(response.status, falcon.HTTP_400)
            mock_neighbors.assert_not_called()
