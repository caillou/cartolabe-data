import pandas as pd
from unittest import TestCase

from ..loading import (load_comma_separated_column, load_identity_column,
                       load_text_column
                       )

from ..projection import umap_projection, lsa_projection


class TestProjection(TestCase):

    def setUp(self):
        df = pd.DataFrame(
            {"authFullName_s": [
                "Philippe Caillou,Samir Aknine,Suzanne Pinson, Samuel Thiriot",
                "Bruno Cessac,Hélène Paugam-Moisy,Thierry Viéville",
                "Zach Lewkovicz,Philippe Caillou,Jean-Daniel Kant",
                "Zach Lewkovicz,Samuel Thiriot"
            ]
            }
        )

        self.authors_tab, self.authors_scores = load_comma_separated_column(
            df, "authFullName_s")

        df = pd.DataFrame(
            {"text": [
                ("Many empirical studies emphasize the role of social "
                 "networks in job search. The social network implicated "
                 "in this process"),
                ("In this short note, we investigate. Responsibility of "
                 "Pseudoknotted"),
                "Exactly Solvable Stochastic Processes for Traffic Modelling",
                ("In the ground case, the procedure terminates and provides a "
                 "decision algorithm for the word problem.")]})

        self.words_tab, self.words_scores = load_text_column(
            df['text'], 4, 1, 1)

        df = pd.DataFrame(
            {"en_title_s": [
                "Multi-prover verification of floating-point programs",
                "Hardware-independent proofs of numerical programs",
                "Viewing a World of Annotations through AnnoVIP",
                "Combinatorial identification problems and graph powers"]
             }
        )

        self.articles_tab, self.articles_scores = load_identity_column(
            df, "en_title_s")

    def test_umap_projection(self):
        projected_umap = umap_projection([self.articles_tab.todense(),
                                          self.authors_tab.todense(),
                                          self.words_tab.todense()])

        self.assertEqual(len(projected_umap), 3)
        self.assertEqual(
            projected_umap[0].shape, (2, len(self.articles_scores)))
        self.assertEqual(
            projected_umap[1].shape, (2, len(self.authors_scores)))
        self.assertEqual(projected_umap[2].shape, (2, len(self.words_scores)))

    def test_lsa_projection(self):
        projected_lsa = lsa_projection(
            2, self.words_tab, [self.articles_tab, self.authors_tab,
                                self.words_tab]
        )

        self.assertEqual(len(projected_lsa), 3)
        self.assertEqual(projected_lsa[0].shape,
                         (2, len(self.articles_scores)))
        self.assertEqual(projected_lsa[1].shape,
                         (2, len(self.authors_scores)))
        self.assertEqual(projected_lsa[2].shape, (2, len(self.words_scores)))
