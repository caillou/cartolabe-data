from unittest import TestCase
from unittest.mock import patch, MagicMock

import numpy as np
import pandas as pd

from ..neighbors import get_neighbors, get_best_neighbors_by_score


class TestNeighbors(TestCase):

    @patch('cartodata.neighbors.nmslib.init')
    def test_get_neighbors(self, mock_nsm_init):
        mock_nsm = MagicMock()
        mock_nsm_init.return_value = mock_nsm
        mock_nsm.knnQueryBatch.return_value = [
            (np.array([1, 3, 0, 2]),
             np.array([0., 0.41262734, 0.42588288, 0.5051584])),
            (np.array([3, 2, 0, 1]),
             np.array([0., 0.23282534, 0.24472147, 0.5081445]))
        ]

        neighbors_matrix = np.array(
            [[0.135139, 0.20937858, 0.41124057],
             [-0.11924833, -0.16273455, -0.08106077]]
        )
        neighbors_scores = pd.Series([9, 1, 7, 5], index=['a', 'b', 'c', 'd'])
        matrices = [
            np.array([[-0.18034233, -0.01504826],
                      [-0.03564123, 0.29224918]]),
            np.array([[0.01871956, 0.0371474],
                      [0.13454162, -0.00455442]])
        ]

        neighbors_ids = get_neighbors(
            neighbors_matrix, neighbors_scores, matrices, n_neighbors=4)

        # Assert nms init
        mock_nsm_init.assert_called_with(method='hnsw', space='cosinesimil')
        mock_nsm.addDataPointBatch.assert_called()
        np.testing.assert_equal(
            mock_nsm.addDataPointBatch.call_args[0][0],
            neighbors_matrix.transpose()
        )
        mock_nsm.createIndex.assert_called_with(print_progress=True)

        # Assert first matrix call
        mock_nsm.knnQueryBatch.assert_called()
        np.testing.assert_equal(
            mock_nsm.knnQueryBatch.call_args_list[0][0][0], matrices[0].T)
        self.assertEqual(mock_nsm.knnQueryBatch.call_args_list[0][1], {
            'k': 4, 'num_threads': 4})

        # Assert second matrix call
        mock_nsm.knnQueryBatch.assert_called()
        np.testing.assert_equal(
            mock_nsm.knnQueryBatch.call_args_list[1][0][0], matrices[1].T)
        self.assertEqual(mock_nsm.knnQueryBatch.call_args_list[1][1], {
            'k': 4, 'num_threads': 4})

        self.assertEqual(len(neighbors_ids), 2)
        np.testing.assert_equal(neighbors_ids[0], np.array(
            [[1, 3], [3, 2], [0, 0], [2, 1]]))
        np.testing.assert_equal(neighbors_ids[1], np.array(
            [[1, 3], [3, 2], [0, 0], [2, 1]]))

    @patch('cartodata.neighbors.nmslib.init')
    @patch('cartodata.neighbors.get_best_neighbors_by_score')
    def test_get_neighbors_power_score(self, mock_get_best, mock_nsm_init):
        mock_nsm = MagicMock()
        mock_nsm_init.return_value = mock_nsm
        mock_nsm.knnQueryBatch.return_value = [
            (np.array([1, 3, 0, 2]),
             np.array([0., 0.41262734, 0.42588288, 0.5051584])),
            (np.array([3, 2, 0, 1]),
             np.array([0., 0.23282534, 0.24472147, 0.5081445]))
        ]
        mock_get_best.return_value = np.array([[4, 5], [2, 2], [1, 0], [3, 4]])

        neighbors_matrix = np.array([[0.135139, 0.20937858, 0.41124057],
                                     [-0.11924833, -0.16273455, -0.08106077]])
        neighbors_scores = pd.Series([9, 1, 7, 5], index=['a', 'b', 'c', 'd'])
        matrices = [
            np.array([[-0.18034233, -0.01504826],
                      [-0.03564123, 0.29224918]]),
            np.array([[0.01871956, 0.0371474],
                      [0.13454162, -0.00455442]])
        ]

        neighbors_ids = get_neighbors(
            neighbors_matrix, neighbors_scores, matrices, power_score=0.5,
            n_neighbors=4
        )

        # Assert nms init
        mock_nsm_init.assert_called_with(method='hnsw', space='cosinesimil')
        mock_nsm.addDataPointBatch.assert_called()
        np.testing.assert_equal(
            mock_nsm.addDataPointBatch.call_args[0][0],
            neighbors_matrix.transpose()
        )
        mock_nsm.createIndex.assert_called_with(print_progress=True)

        # Assert first matrix call
        mock_nsm.knnQueryBatch.assert_called()
        np.testing.assert_equal(
            mock_nsm.knnQueryBatch.call_args_list[0][0][0], matrices[0].T)
        self.assertEqual(mock_nsm.knnQueryBatch.call_args_list[0][1], {
            'k': 100, 'num_threads': 4})

        # Assert second matrix call
        mock_nsm.knnQueryBatch.assert_called()
        np.testing.assert_equal(
            mock_nsm.knnQueryBatch.call_args_list[1][0][0], matrices[1].T)
        self.assertEqual(mock_nsm.knnQueryBatch.call_args_list[1][1], {
            'k': 100, 'num_threads': 4})

        self.assertEqual(len(neighbors_ids), 2)
        np.testing.assert_equal(neighbors_ids[0], np.array(
            [[4, 5], [2, 2], [1, 0], [3, 4]]))
        np.testing.assert_equal(neighbors_ids[1], np.array(
            [[4, 5], [2, 2], [1, 0], [3, 4]]))

    def test_get_best_neighbors_by_score(self):
        neighbors = [
            (np.array([1, 3, 0, 2]),
             np.array([0., 0.41262734, 0.42588288, 0.5051584])),
            (np.array([3, 2, 0, 1]),
             np.array([0., 0.23282534, 0.24472147, 0.5081445]))
        ]
        neighbors_scores = pd.Series([9, 1, 7, 5], index=['a', 'b', 'c', 'd'])
        n_neighbors = 2

        neighbor_ids = get_best_neighbors_by_score(
            neighbors, neighbors_scores, n_neighbors)

        np.testing.assert_equal(neighbor_ids[:, 0], [1, 0])
        np.testing.assert_equal(neighbor_ids[:, 1], [3, 0])
