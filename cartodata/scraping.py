import logging
import random
import re
import string
import urllib.parse
from random import randint
from time import sleep

import requests
import pandas as pd

logger = logging.getLogger(__name__)

ALPHANUMERIC_REGEX = re.compile('[a-zA-Z0-9 ]+')


class HalScrapingException(Exception):
    pass


def build_query_param(param, value):
    return param + '=' + str(value)


def build_query_url(base_url, query, fields, filters, rows, cursor_mark, sort):
    params = [build_query_param('q', query)]

    if fields:
        params.append(build_query_param('fl', ','.join(fields)))

    if filters:
        for filter, value in filters.items():
            params.append(build_query_param('fq', filter + ':' + value))

    params.append(build_query_param('rows', rows))
    params.append(build_query_param(
        'cursorMark', urllib.parse.quote_plus(cursor_mark)))
    params.append(build_query_param('sort', sort))

    return base_url + '/?' + '&'.join(params)


def do_cool_down(max_seconds):
    if max_seconds > 0:
        sleep(randint(1, max_seconds))


def scrape_hal(struct, filters, years, cool_down=0, csv_file=None):
    """
    Scrape the HAL database, requesting documents produced during the given
    years, matching the filters. This function returns a pandas DataFrame, with
    fields as columns.

    :param struct: acronym for the struct
    :param filters: a dictionnary of filters. Provide an empty dictionnary to
    not filter
    :param years: an array of years
    :param cool_down: an integer of seconds to cooldown between each request
    sent to HAL
    :param csv_file:
    :return:
    """
    base_url = 'http://api.archives-ouvertes.fr/search/'
    fields = ['structId_i',
              'authFullName_s',
              'en_abstract_s',
              'en_keyword_s',
              'en_title_s',
              'structAcronym_s',
              'producedDateY_i',
              'producedDateM_i',
              'halId_s',
              'docid',
              'en_domainAllCodeLabel_fs']
    query = '*:*'
    rows = '10000'
    sort = 'docid asc'

    df = pd.DataFrame(None, columns=fields)
    if csv_file is not None:
        df.to_csv(csv_file)

    logger.debug('Starting mining task from HAL...')
    for year in years:
        documents = []
        cursor_mark = '*'
        filters['producedDateY_i'] = str(year)
        done = False
        first = True

        while not done:
            do_cool_down(cool_down)
            url = build_query_url(base_url, query, fields, filters,
                                  rows, cursor_mark, sort)
            r = requests.get(url)

            if r.status_code != 200:
                raise HalScrapingException(
                    'A request sent to HAL returned an invalid status code. '
                    f'STATUS CODE: {r.status.code}, URL: {url}'
                )

            r_json = r.json()
            data = r_json['response']
            if first:
                logger.debug(
                    f"Importing {data['numFound']} docs for year {year}"
                )
            first = False
            documents.extend(filter_docs(struct, data['docs']))
            if cursor_mark == r_json['nextCursorMark']:
                done = True
            cursor_mark = r_json['nextCursorMark']

        year_df = pd.DataFrame(documents, columns=fields)
        df = pd.concat([df, year_df])
        if csv_file is not None:
            year_df.to_csv(csv_file, mode='a', header=False)

    return df


def is_empty(phrase):
    if not phrase:
        return True
    return len(phrase.strip(' ')) == 0


def word_count(phrase):
    if not phrase:
        return 0
    return len(phrase.split(' '))


def is_title_not_empty(doc):
    """
    Checks the title is not empty. HAL returns a list of titles for the field
    "en_title_s", we only use the first element of that list.

    :param doc:
    :return:
    """
    return has_field(doc, 'en_title_s') and not is_empty(doc['en_title_s'][0])


def is_abstract_long_enough(doc, min_length=15):
    """
    Checks the abstract is not empty and at least a minimum number of
    words. HAL returns a list of abstracts for the field "en_abstract_s", we
    only use the first element of that list.

    :param min_length:
    :param doc:
    :return:
    """
    has_abstract = has_field(doc, 'en_abstract_s')
    return has_abstract and word_count(doc['en_abstract_s'][0]) > min_length


def has_field(doc, field):
    return field in doc and len(doc[field]) > 0


def filter_docs(struct, docs):
    join_fields = ['authFullName_s', 'en_keyword_s', 'structAcronym_s']
    valid_docs = []

    for doc in docs:
        if is_title_not_empty(doc) and is_abstract_long_enough(doc):
            doc['en_title_s'] = doc['en_title_s'][0]
            doc['en_abstract_s'] = doc['en_abstract_s'][0]

            if has_field(doc, 'structAcronym_s'):
                doc['structAcronym_s'].append(struct.upper())
            for field in join_fields:
                if field in doc:
                    doc[field] = ','.join(doc[field])
            valid_docs.append(doc)

    return valid_docs


def _process_domain_string(domain_string):
    if domain_string and len(domain_string) > 0:
        facet_string = domain_string[-1]
        facet_index = facet_string.rfind('FacetSep_')

        if facet_index > 0:
            facet_string = facet_string[facet_index + 9:]

        domains = ALPHANUMERIC_REGEX.findall(facet_string)
        domains = list({x.strip() for x in domains if len(x) > 5})
        return ','.join(domains)

    return ''


def rand_str_generator(size=6, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))


def process_domain_column(df):
    if 'en_domainAllCodeLabel_fs' in df:
        df['en_domainAllCodeLabel_fs'] = df['en_domainAllCodeLabel_fs'].fillna(
            '').map(_process_domain_string)
