import logging.config
import os

import click
import gunicorn.app.base
from gunicorn import util

from cartodata.logs import LOGGING
from cartodata.scraping import scrape_hal, process_domain_column
from cartodata.workflows.arxiv import arxiv_workflow
from cartodata.workflows.bibliolabs import bib_workflow
from cartodata.workflows.debat import (
    debat_workflow, debatS_workflow, debatE_workflow, debatO_workflow,
    debatD_workflow, debatF_workflow
)
from cartodata.workflows.hal import (
    lri_workflow, inria_workflow, hal_workflow, ups_workflow
)
from cartodata.workflows.softwareh import software_workflow
from cartodata.workflows.wikipedia import wiki_workflow

logging.config.dictConfig(LOGGING)

logger = logging.getLogger(__name__)

# Below are structId_i's displayed in
# https://aurehal.archives-ouvertes.fr/structure/
# all IDs are extracted by keyword search for institutions/laboratory and
# structures valides
STRUCT_MAP = {
    # https://aurehal.archives-ouvertes.fr/structure/browse/critere/cnrs/solR/1/page/1/nbResultPerPage/50/tri/valid/filter/valid/category/institution
    "cnrs":
    # cnrs, inst.
    "441569 302193 558175 300046 341038 302108 1095810 300045 347129 302050",
    # https://aurehal.archives-ouvertes.fr/structure/browse/critere/inria/solR/1/page/1/nbResultPerPage/50/tri/valid/filter/valid/category/institution
    "inria": "300009",   # inria, inst.
    # https://aurehal.archives-ouvertes.fr/structure/browse?critere=name_s:%22Laboratoire+de+Recherche+en+Informatique%22
    "lri": "1050003 2544",  # lri, lab, structures fermées
    # https://aurehal.archives-ouvertes.fr/structure/browse?critere=lisn&category=laboratory
    "lisn": "1061259",  # lisn, lab, structures valides
    # https://aurehal.archives-ouvertes.fr/structure/browse/critere/univ%C3%A9rsit%C3%A9+paris+saclay/solR/1/page/1/nbResultPerPage/50/tri/valid/filter/valid/category/institution
    "ups": "419361 1073884 1062302 1066727",  # univérsité paris saclay, inst.
}

WORKFLOWS = {'lri': lri_workflow,
             'inria': inria_workflow,
             'hal': hal_workflow,
             'wiki': wiki_workflow,
             'debat': debat_workflow,
             'debatS': debatS_workflow,
             'debatD': debatD_workflow,
             'debatF': debatF_workflow,
             'debatO': debatO_workflow,
             'debatE': debatE_workflow,
             'software': software_workflow,
             'arxiv': arxiv_workflow,
             'bib': bib_workflow,
             'ups': ups_workflow}


@click.group()
def main():
    """
    Simple CLI for cartolabe-data
    """
    pass


@main.command()
@click.argument('dataset', type=click.Choice(WORKFLOWS.keys()))
@click.option('-o', '--output', default='dumps', help='The output directory.')
def workflow(dataset, output):
    """
    Run through one of the predefined workflows.
    """
    if not os.path.exists(output):
        os.mkdir(output)

    logger.info(
        f'Starting workflow {dataset} with output to {output}'
    )
    WORKFLOWS[dataset](dump_dir=output)


@main.command()
@click.option('-f', '--yearfrom', default=2021, help=(
    'Fetch only articles published after this year (included).'
), type=click.IntRange(min=1990, max=2030))
@click.option('-t', '--yearto', default=2022, help=(
    'Fetch only articles published up to this year (not included).'
), type=click.IntRange(min=1990, max=2030))
@click.option('-s', '--struct', default=None, help=(
    'Fetch only publications linked to this organization.'
    f' Organization can be one of {",".join(STRUCT_MAP.keys())}'
), type=click.STRING)
def fetch_data(yearfrom, yearto, struct):
    """
    Fetch scientific publications from HAL.
    """
    filters = {}

    if struct:
        filters['structId_i'] = "(" + STRUCT_MAP[struct.lower()] + ")"

    years = range(yearfrom, yearto)

    df = scrape_hal(struct, filters, years, cool_down=2)
    process_domain_column(df)

    # Save the dataframe into a csv file
    if not struct:
        struct = 'hal'
    filename = f'datas/{struct.lower()}_{yearfrom}_{yearto - 1}.csv'

    df.to_csv(filename)
    return df


@main.command()
@click.option('-h', '--host', default='127.0.0.1', help='Hostname',
              type=click.STRING)
@click.option('-p', '--port', default=7000, help='Port number', type=click.INT)
def runserver(host, port):
    default_settings = 'cartodata.api.settings'
    os.environ.setdefault('CARTODATA_CONFIG', default_settings)
    settings_module = os.environ.get('CARTODATA_CONFIG')
    print(f'Settings "{settings_module}"')

    application = 'cartodata.api.wsgi'
    options = {
        'bind': '%s:%s' % (host, port),
        'timeout': '60',
        'workers': 1,
        'threads': 3,
        'access-logfile': '-',
        'log-file': '-',
        'log-level': 'DEBUG',
        'reload': True
    }
    GunicornServer(application, options).run()
    # httpd = simple_server.make_server('127.0.0.1', 7000, create_api())
    # httpd.serve_forever()


class GunicornServer(gunicorn.app.base.BaseApplication):

    def __init__(self, app_uri: str, options=None):
        self.options = options or {}
        self.app_uri = app_uri
        super(GunicornServer, self).__init__()

    def load_config(self):
        config = dict([(key, value) for key, value in self.options.items()
                       if key in self.cfg.settings and value is not None])
        for key, value in config.items():
            self.cfg.set(key.lower(), value)

    def load(self):
        return util.import_app(self.app_uri)


if __name__ == "__main__":
    main()
