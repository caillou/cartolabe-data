import numpy as np
import pandas as pd
from sklearn.cluster import KMeans
from sklearn.preprocessing import binarize

from cartodata.logs import timeit


@timeit
def create_kmeans_clusters(nb_clusters, clustering_table, naming_table,
                           natural_space_naming_table, naming_scores,
                           previous_cluster_labels, naming_profile_table=None,
                           label_length=2, max_iter=100, n_init=1,
                           weight_name_length=0.5):
    """
    Create a correspondance matrix for nb_clusters clusters. Clusters are
    created using the KMeans algorithm.

    :param nb_clusters: The number of clusters to create
    :param clustering_table: The matrix to fit to create the clusters
    :param naming_table: The matrix used to name clusters. This matrix should
    have the same number of rows as clustering_table
    :param natural_space_naming_table: The naming_table in it's natural space.
    It should have the same number of columns as naming_table
    :param naming_scores: The scores for the naming_table columns
    :param previous_cluster_labels: A list of previous cluster labels, used to
    prevent duplicates in naming
    :param naming_profile_table: An optional naming_table in projected space
    to build the cluster's profile with. If it is not provided, the
    naming_table will be used instead.
    :param label_length:
    :param max_iter:
    :param n_init:
    :return:
    """
    km = KMeans(n_clusters=nb_clusters, init='k-means++', max_iter=max_iter,
                n_init=n_init, verbose=0)
    km.fit(clustering_table.T)
    cluster_prediction = km.predict(clustering_table.T)

    binary_naming_table = binarize(natural_space_naming_table)

    # Name clusters
    names_prediction = km.predict(naming_table.T)
    naming_profile = naming_table

    if naming_profile_table is not None:
        naming_profile = naming_profile_table

    cluster_scores = pd.Series(dtype=np.float64)
    cluster_profiles = []

    for cluster_idx in range(nb_clusters):
        cluster_labels = []
        cluster_labels_indices = []

        # Vector of articles in cluster
        articles_in_cluster = np.where(cluster_prediction == cluster_idx, 1, 0)
        names_in_cluster = np.where(names_prediction == cluster_idx, 1, 0)

        for i in range(label_length):
            nb_of_articles_in_cluster_per_word = (
                articles_in_cluster * binary_naming_table
            )
            max_nb_articles_in_cluster = np.sum(articles_in_cluster)
            nb_of_articles_outside_cluster_per_word = (
                (1 - articles_in_cluster) * binary_naming_table
            )
            max_nb_of_articles_outside_cluster = np.sum(
                1 - articles_in_cluster)

            score_for_all_words = (
                (nb_of_articles_in_cluster_per_word /
                 max_nb_articles_in_cluster) -
                (nb_of_articles_outside_cluster_per_word /
                 max_nb_of_articles_outside_cluster)
            )
            score_for_words_in_cluster = score_for_all_words * names_in_cluster

            label_idx, label_name = choose_best_cluster_label(
                previous_cluster_labels, score_for_words_in_cluster,
                naming_scores
            )
            cluster_labels.append(label_name)
            cluster_labels_indices.append(label_idx)
            previous_cluster_labels.append(label_name)

            # If more than 75% articles are labeled by this term, stop
            prop_articles_labeled = (
                nb_of_articles_in_cluster_per_word[label_idx] /
                max_nb_articles_in_cluster
            )
            if prop_articles_labeled > 0.75:
                break

            # Otherwirse, remove articles in cluster that are labeled by this
            # term
            articles_in_cluster = articles_in_cluster - (
                articles_in_cluster *
                np.ravel(binary_naming_table.T[label_idx, :].todense())
            )

        cluster_label = ", ".join(cluster_labels)
        cluster_scores[cluster_label] = np.sum(
            naming_scores[cluster_labels_indices]
        )
        cluster_profile = (
            np.sum(naming_profile[:, cluster_labels_indices], axis=1) /
            len(cluster_labels_indices)
        )
        cluster_profiles.append(cluster_profile)

    return (
        np.vstack(cluster_profiles).T, km.cluster_centers_.T, cluster_scores,
        km
    )


def choose_best_cluster_label(previous_cluster_labels,
                              score_for_words_in_cluster, naming_scores,
                              weight_name_length=0.5):
    """
    Pick the best label to name a cluster. We look at the 20 best labels by
    score, and update their score to favor longer n-grams. We then pick the
    first label which is not in previous_cluster_labels.

    :param previous_cluster_labels:
    :param score_for_words_in_cluster:
    :param naming_scores:
    :return:
    """
    best_words_in_cluster = np.argsort(score_for_words_in_cluster)[::-1]

    # Look at the best 20 words for the cluster
    sample_size = 20

    # Update their score based on word length
    updated_scores = [(
        score_for_words_in_cluster[idx] *
        (len(naming_scores.index[idx].split(" ")) ** weight_name_length)
    ) for idx in best_words_in_cluster[:sample_size]
    ]
    updated_best_words_in_cluster = np.argsort(updated_scores)[::-1]
    label = None
    label_idx = 0
    for idx in updated_best_words_in_cluster:
        name = naming_scores.index[best_words_in_cluster[idx]]
        if name not in previous_cluster_labels:
            label = name
            label_idx = idx
            break

    # If no name worked, take the next best not already used...
    while label is None and sample_size < len(best_words_in_cluster):
        name = naming_scores.index[best_words_in_cluster[sample_size]]

        if name not in previous_cluster_labels:
            label = name
            label_idx = sample_size
        sample_size += 1

    return best_words_in_cluster[label_idx], label
